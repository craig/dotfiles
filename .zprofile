eval "$(/opt/homebrew/bin/brew shellenv)"

# Setup XDG variable defaults
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"
export XDG_CONFIG_DIRS="/etc/xdg"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
#export XDG_RUNTIME_DIR="" # This one is non-trivial to set and a PITA to actually get right, so just skipping it for now (see https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html#variables)

# Rancher Desktop
export PATH="${HOME}/.rd/bin:$PATH"
