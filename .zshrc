# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.omz"

# Set name of the theme to load --- if set to 'random', it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME='powerlevel10k/powerlevel10k'

# Set the auto-update behavior
zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Configure auto-update frequency (in days).
zstyle ':omz:update' frequency 7

# Set timestamp format for command history output
# Can be one of three optional formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Configure ssh-agent
#zstyle :omz:plugins:ssh-agent agent-forwarding yes
zstyle :omz:plugins:ssh-agent quiet yes                           # Suppress output
zstyle :omz:plugins:ssh-agent identities id_ecdsa google_compute_engine
zstyle :omz:plugins:ssh-agent lifetime 8h                         # Remove identities after 8h
zstyle :omz:plugins:ssh-agent ssh-add-args --apple-load-keychain  # Store passphrase in login keychain


# Plugins
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(aliases common-aliases command-not-found direnv \
  ssh-agent git git-extras gitignore aws gcloud \
  tmux vi-mode zsh-autosuggestions zsh-syntax-highlighting fzf \
)

# Configure tmux
ZSH_TMUX_AUTOSTART='false'
ZSH_TMUX_AUTOSTART_ONCE='true'
ZSH_TMUX_AUTOCONNECT='false'
ZSH_TMUX_AUTOQUIT="$ZSH_TMUX_AUTOSTART"
ZSH_TMUX_CONFIG="$HOME/.config/tmux/tmux.conf"
#ZSH_TMUX_AUTONAME_SESSION='false'
#ZSH_TMUX_DEFAULT_SESSION_NAME='default'
ZSH_TMUX_DETACHED='true'
#ZSH_TMUX_FIXTERM='true'
#ZSH_TMUX_FIXTERM_WITHOUT_256COLOR 	## $TERM to use for non 256-color terminals (default: tmux if available, screen otherwise)
#ZSH_TMUX_FIXTERM_WITH_256COLOR     ## $TERM to use for 256-color terminals (default: tmux-256color if available, screen-256color otherwise)
#ZSH_TMUX_ITERM2='true'
ZSH_TMUX_UNICODE='true'

# Configure vi-mode
VI_MODE_RESET_PROMPT_ON_MODE_CHANGE=true

# Do the thing!
source $ZSH/oh-my-zsh.sh

# CTRL-Space to accept autosuggestion
bindkey '^ ' autosuggest-accept


####################
# User configuration
####################

# export MANPATH="/usr/local/man:$MANPATH"

# Shell history
setopt SHARE_HISTORY      # share history across multiple zsh sessions
setopt APPEND_HISTORY     # append to history
setopt INC_APPEND_HISTORY # adds commands as they are typed, not at shell exit
setopt HIST_IGNORE_DUPS   # do not store duplications
setopt HIST_FIND_NO_DUPS  # ignore duplicates when searching
setopt HIST_REDUCE_BLANKS # removes blank lines from history

# Does anybody even use the ISO locales anymore?
export LANG=en_US.UTF-8

# Preferred editor is neovim
export EDITOR='nvim'

# Configure syntax highlighting for less
export LESSOPEN="| /opt/homebrew/bin/pygmentize -O style='gruvbox-dark' %s"
export LESS=' --no-init --quit-if-one-screen -R '

# Powerlevel10k
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Use mise-en-place for general tool version management
eval "$(mise activate zsh)"

# Configure color output for ls (gruvbox-dark theme)
d="$HOME/.dircolors"
test -r "$d" && eval "$(gdircolors "$d")"

# Configure eza (better ls)
alias eza="eza --color=always --icons=always"
alias ls='eza'
alias l='eza -1'
alias la='eza --header --git --all'
alias ll='eza --long --header --git'
alias lla='ll --all'
alias lt='eza --long --header --git --tree --level 2'

# Configure zoxide (better cd)
eval "$(zoxide init zsh)"
alias cd="z"

# Configure bat (better cat)
export BAT_THEME=gruvbox-dark
alias cat=bat

# Configure fzf
export FZF_DEFAULT_COMMAND='fd --hidden --strip-cwd-prefix --exclude .git'
export FZF_DEFAULT_OPTS='--tmux 80% --color=bg+:#4c3836,bg:#32302f,spinner:#fb4934,hl:#928374,fg:#ebdbb2,header:#928374,info:#8ec07c,pointer:#fb4934,marker:#fb4934,fg+:#ebdbb2,prompt:#fb4934,hl+:#fb4934' # Open results in tmux pane and use gruvbox theme
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd --type=d --hidden --strip-cwd-prefix --exclude .git"
export DISABLE_FZF_KEY_BINDINGS='false'

show_file_or_dir_preview="if [ -d {} ]; then eza --tree --color=always {} | head -200; else bat -n --color=always --line-range :500 {}; fi"

export FZF_CTRL_T_OPTS="--preview '$show_file_or_dir_preview'"
export FZF_ALT_C_OPTS="--preview 'eza --tree --color=always {} | head -200'"

# Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf --preview 'eza --tree --color=always {} | head -200' "$@" ;;
    export|unset) fzf --preview "eval 'echo ${}'"         "$@" ;;
    ssh)          fzf --preview 'dig {}'                   "$@" ;;
    *)            fzf --preview "$show_file_or_dir_preview" "$@" ;;
  esac
}

# Use fd (https://github.com/sharkdp/fd) for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden --exclude .git . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type=d --hidden --exclude .git . "$1"
}

# Configure fzf-git
[[ -d ~/fzf-git.sh ]] && source ~/fzf-git.sh/fzf-git.sh

# Let gcloud use system site-packages
export CLOUDSDK_PYTHON_SITEPACKAGES=1

# Configure gitlab-ci-local
source ~/.config/gitlab-ci-local/init.sh
