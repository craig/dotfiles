" vim:set ft=vim sw=2 ts=2 sts=2 et tw=98 foldmarker={,} foldlevel=0 foldmethod=marker spell:

" Setup Plug Manager {
  " Load vim-plug
  if empty(glob("~/.vim/autoload/plug.vim"))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.github.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif

  " Specify a directory for plugins
  call plug#begin('~/.vim/plugged')
" }

" Base {
  Plug 'tpope/vim-sensible'
" }

" General {
  Plug 'mileszs/ack.vim'                " Better search than grep
  Plug 'terryma/vim-multiple-cursors'   " Do magic multi-edits
  Plug 'scrooloose/nerdtree'            " Project drawer / file explorer
  Plug 'dhruvasagar/vim-vinegar'        " Fork of vinegar for nerdtree
  Plug 'christoomey/vim-tmux-navigator' " Extend split navigation keys to tmux panes
  Plug 'freitass/todo.txt-vim'          " Work with todo.txt files
" }

" UI & Themes {
  " UI
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'powerline/fonts'
  Plug 'Yggdroot/indentLine'
  Plug 'mhinz/vim-signify'
  Plug 'skielbasa/vim-material-monokai'
  Plug 'wesQ3/vim-windowswap'

  " Themes
  Plug 'ayu-theme/ayu-vim'
  Plug 'morhetz/gruvbox'
  Plug 'raphamorim/lucario'
  Plug 'sirakij/macvim-light'
  Plug 'tomasr/molokai'
  Plug 'acoustichero/simple_dark'
  Plug 'roosta/srcery'
  Plug 'lifepillar/vim-solarized8'
  Plug 'jacoborus/tender'
  Plug 'zcodes/vim-colors-basic'
  Plug 'agude/vim-eldar'
  Plug 'rakr/vim-one'
" }

" Markdown {
  Plug 'tpope/vim-markdown'
  Plug 'JamshedVesuna/vim-markdown-preview'
  Plug 'rhysd/vim-gfm-syntax'
" }

" Writing {
  Plug 'reedes/vim-wordy'
" }

" Programming {
  " General
  Plug 'tpope/vim-surround'
  Plug 'scrooloose/syntastic'
  Plug 'scrooloose/nerdcommenter'
  Plug 'tpope/vim-fugitive'
  Plug 'junegunn/vim-easy-align'

  " Bash
  Plug 'z0mbix/vim-shfmt'

  " Chef
  Plug 'vadv/vim-chef'
  Plug 'hron84/vim-librarian'

  " Go Lang {
  Plug 'fatih/vim-go', { 'tag': '*' }

  " HCL
  Plug 'hashivim/vim-terraform'

  " Ruby
  Plug 'vim-scripts/ruby-matchit'
" }

" Initialize plugins {
  call plug#end()
" }

""""     " Programming {
""""             " Pick one of the checksyntax, jslint, or syntastic
""""             Plug 'mattn/webapi-vim'
""""             Plug 'mattn/gist-vim'
""""             Plug 'tpope/vim-commentary'
""""             Plug 'godlygeek/tabular'
""""             Plug 'luochen1990/rainbow'
""""             if executable('ctags')
""""                 Plug 'majutsushi/tagbar'
""""             endif
""""     " }

""""     " General {
""""         if count(g:spf13_bundle_groups, 'general')
""""             Plug 'altercation/vim-colors-solarized'
""""             Plug 'spf13/vim-colors'
""""             Plug 'tpope/vim-repeat'
""""             Plug 'rhysd/conflict-marker.vim'
""""             Plug 'jiangmiao/auto-pairs'
""""             Plug 'ctrlpvim/ctrlp.vim'
""""             Plug 'tacahiroy/ctrlp-funky'
""""             Plug 'vim-scripts/sessionman.vim'
""""             Plug 'matchit.zip'
""""             Plug 'bling/vim-bufferline'
""""             Plug 'easymotion/vim-easymotion'
""""             Plug 'jistr/vim-nerdtree-tabs'
""""             Plug 'flazz/vim-colorschemes'
""""             Plug 'mbbill/undotree'
""""             Plug 'nathanaelkane/vim-indent-guides'
""""             if !exists('g:spf13_no_views')
""""                 Plug 'vim-scripts/restore_view.vim'
""""             endif
""""             Plug 'tpope/vim-abolish.git'
""""             Plug 'osyo-manga/vim-over'
""""             Plug 'kana/vim-textobj-user'
""""             Plug 'kana/vim-textobj-indent'
""""             Plug 'gcmt/wildfire.vim'
""""         endif
""""     " }

""""     " Snippets & AutoComplete {
""""         if count(g:spf13_bundle_groups, 'snipmate')
""""             Plug 'garbas/vim-snipmate'
""""             Plug 'honza/vim-snippets'
""""             " Source support_function.vim to support vim-snippets.
""""             if filereadable(expand("~/.vim/bundle/vim-snippets/snippets/support_functions.vim"))
""""                 source ~/.vim/bundle/vim-snippets/snippets/support_functions.vim
""""             endif
""""         elseif count(g:spf13_bundle_groups, 'youcompleteme')
""""             Plug 'Valloric/YouCompleteMe'
""""             Plug 'SirVer/ultisnips'
""""             Plug 'honza/vim-snippets'
""""         elseif count(g:spf13_bundle_groups, 'neocomplcache')
""""             Plug 'Shougo/neocomplcache'
""""             Plug 'Shougo/neosnippet'
""""             Plug 'Shougo/neosnippet-snippets'
""""             Plug 'honza/vim-snippets'
""""         elseif count(g:spf13_bundle_groups, 'neocomplete')
""""             Plug 'Shougo/neocomplete.vim.git'
""""             Plug 'Shougo/neosnippet'
""""             Plug 'Shougo/neosnippet-snippets'
""""             Plug 'honza/vim-snippets'
""""         endif
""""     " }

""""     " Python {
""""         if count(g:spf13_bundle_groups, 'python')
""""             " Pick either python-mode or pyflakes & pydoc
""""             Plug 'klen/python-mode'
""""             Plug 'yssource/python.vim'
""""             Plug 'python_match.vim'
""""             Plug 'pythoncomplete'
""""         endif
""""     " }

""""     " Javascript {
""""         if count(g:spf13_bundle_groups, 'javascript')
""""             Plug 'elzr/vim-json'
""""             Plug 'groenewege/vim-less'
""""             Plug 'pangloss/vim-javascript'
""""             Plug 'briancollins/vim-jst'
""""             Plug 'kchmck/vim-coffee-script'
""""         endif
""""     " }

""""     " HTML {
""""         if count(g:spf13_bundle_groups, 'html')
""""             Plug 'amirh/HTML-AutoCloseTag'
""""             Plug 'hail2u/vim-css3-syntax'
""""             Plug 'gorodinskiy/vim-coloresque'
""""             Plug 'tpope/vim-haml'
""""             Plug 'mattn/emmet-vim'
""""         endif
""""     " }
" }
