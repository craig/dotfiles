alias gcl='gitlab-ci-local'

# Overrides .gitlab-ci.yml as the default git ci/cd file
#FILE='.gitlab-ci-local.yml' # --file

# Set container runtime
export GCL_CONTAINER_EXECUTABLE=podman

# Always runs needed jobs, when gitlab-ci-local <job-name> is called
export GCL_NEEDS='true'

# Logging options
export GCL_TIMESTAMPS=true         # or --timestamps: show timestamps in logs
#export GCL_MAX_JOB_NAME_PADDING=30 # or --maxJobNamePadding: limit padding around job name
#export GCL_QUIET=true              # or --quiet: Suppress all job output

#compdef gitlab-ci-local
###-begin-gitlab-ci-local-completions-###
#
# yargs command completion script
#
# Installation: /opt/homebrew/bin/gitlab-ci-local completion >> ~/.zshrc
#    or /opt/homebrew/bin/gitlab-ci-local completion >> ~/.zprofile on OSX.
#
_gitlab-ci-local_yargs_completions()
{
  local reply
  local si=$IFS
  IFS=$'
' reply=($(COMP_CWORD="$((CURRENT-1))" COMP_LINE="$BUFFER" COMP_POINT="$CURSOR" /opt/homebrew/bin/gitlab-ci-local --get-yargs-completions "${words[@]}"))
  IFS=$si
  _describe 'values' reply
}
compdef _gitlab-ci-local_yargs_completions gitlab-ci-local
###-end-gitlab-ci-local-completions-###

