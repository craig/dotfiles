return {
  "nvim-lualine/lualine.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  config = function()
    local lualine = require("lualine")
    local lazy_status = require("lazy.status") -- to configure lazy pending updates count
    local aw_section = {
      function()
        return require("aw_watcher").is_connected() and "" or ""
      end,
      cond = function()
        local has_aw, _ = pcall(require, "aw_watcher")
        return has_aw
      end,
    }

    -- configure lualine with modified theme
    lualine.setup({
      options = {
        theme = "gruvbox",
      },
      sections = {
        lualine_x = {
          {
            lazy_status.updates,
            cond = lazy_status.has_updates,
            color = { fg = "#ff9e64" },
          },
          { "encoding" },
          { "fileformat" },
          { "filetype" },
          aw_section,
        },
      },
    })
  end,
}
