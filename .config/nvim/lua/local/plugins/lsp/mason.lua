return {
  "williamboman/mason.nvim",
  dependencies = {
    "williamboman/mason-lspconfig.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
  },
  config = function()
    -- import mason
    local mason = require("mason")

    -- import mason-lspconfig
    local mason_lspconfig = require("mason-lspconfig")

    local mason_tool_installer = require("mason-tool-installer")

    -- enable mason and configure icons
    mason.setup({
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗",
        },
      },
    })

    mason_lspconfig.setup({
      -- list of servers for mason to install
      ensure_installed = {
        "ansiblels",
        "autotools_ls",
        "bashls",
        "dockerls",
        "docker_compose_language_service",
        "golangci_lint_ls",
        "gopls",
        "html",
        "helm_ls",
        "jsonls",
        "jinja_lsp",
        "jsonnet_ls",
        "ltex",
        "lua_ls",
        "markdown_oxide",
        "pyright",
        "ruby_lsp",
        "sqlls",
        "tailwindcss",
        "taplo",
        "terraformls",
        "typos_lsp",
        "yamlls",
      },
    })

    mason_tool_installer.setup({
      ensure_installed = {
        "alex", -- Language choice and tone, sensitivity, etc.
        "ansible-lint",
        "black", -- python formatter
        "checkmake",
        "gitleaks",
        "goimports",
        "golines",
        "gotests",
        "isort", -- python formatter
        "jsonlint",
        "markdownlint",
        "markdownlint-cli2",
        "prettier",
        "pylint",
        "rubocop",
        "shellcheck",
        "shellharden",
        "shfmt",
        "stylua",
        "tflint", -- tofu/terraform linter
        "yamllint",
      },
    })
  end,
}
