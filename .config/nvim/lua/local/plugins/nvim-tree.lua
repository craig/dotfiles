return {
  "nvim-tree/nvim-tree.lua",
  dependencies = "nvim-tree/nvim-web-devicons",
  config = function()
    local nvimtree = require("nvim-tree")

    vim.g.loaded_netrw = 1
    vim.g.loaded_netrwPlugin = 1

    nvimtree.setup({
      view = {
        width = 35,
        relativenumber = true,
      },
      renderer = {
        highlight_git = "all",
        icons = {
          glyphs = {
            folder = {
              arrow_closed = "", -- arrow when folder is closed
              arrow_open = "", -- arrow when folder is open
            },
          },
        },
      },
      -- disable window_picker for
      -- explorer to work well with
      -- window splits
      actions = {
        open_file = {
          window_picker = {
            enable = false,
          },
        },
      },
      filters = {
        custom = { ".DS_Store" },
      },
      git = {
        ignore = false,
      },
    })

    -- custom mappings
    local keymap = vim.keymap.set

    keymap("n", "<leader>ee", ":NvimTreeToggle<CR>", { desc = "Toggle file explorer" })
    keymap("n", "<leader>ef", ":NvimTreeFindFileToggle<CR>", { desc = "Toggle file explorer on current file" })
    keymap("n", "<leader>ec", ":NvimTreeCollapse<CR>", { desc = "Collapse file explorer" })
    keymap("n", "<leader>er", ":NvimTreeRefresh<CR>", { desc = "Refresh file explorer" })
    -- keymap('n', '<C-t>', api.tree.change_root_to_parent, { desc = "Change root to parent directory" })
    -- keymap('n', 'C', api.tree.change_root_to_node,        opts('Cd'))
    -- keymap('n', '?', nvimtree.api.tree.toggle_help, { desc = "Show help message" })
    -- keymap('n', 'v', nvimtree.api.node.open.vertical, { desc = 'Open file in a vertical split' })
    -- keymap('n', 's', nvimtree.api.node.open.horizontal, { desc = 'Open file in a horizontal split' })
  end,
}
