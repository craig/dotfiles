return {
  "stevearc/conform.nvim",
  event = { "BufReadPre", "BufNewFile" },
  config = function()
    local conform = require("conform")

    conform.setup({
      formatters_by_ft = {
        css = { "prettier" },
        graphql = { "prettier" },
        html = { "prettier" },
        javascript = { "prettier" },
        javascriptreact = { "prettier" },
        json = { "prettier" },
        jsonnet = { "jsonnetfmt" },
        libsonnet = { "jsonnetfmt" },
        liquid = { "prettier" },
        lua = { "stylua" },
        markdown = { "prettier" },
        packer = { "packer_fmt" },
        python = { "isort", "black" },
        svelte = { "prettier" },
        terraform = { "tofu-fmt" },
        tf = { "tofu-fmt" },
        tofu = { "tofu-fmt" },
        typescript = { "prettier" },
        typescriptreact = { "prettier" },
        yaml = { "prettier" },
      },
      format_on_save = {
        lsp_fallback = true,
        async = false,
        timeout_ms = 1000,
      },
    })

    vim.keymap.set({ "n", "v" }, "<leader>mp", function()
      conform.format({
        lsp_fallback = true,
        async = false,
        timeout_ms = 1000,
      })
    end, { desc = "Format file or range (in visual mode)" })
  end,
}
