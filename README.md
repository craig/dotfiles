# Dotfiles at Work

[[_TOC_]]

## Overview

This project contains non-sensitive configuration files, scripts, etc. that I use for my work computers, which are managed using [yadm](https://yadm.io/). Of particular note, is that utility's ability to [bootstrap a system](https://yadm.io/docs/bootstrap).

## Bootstrap

To review or update the bootstrap script, see [`.config/yadm/bootstrap`](https://gitlab.com/craig/dotfiles/-/blob/main/.config/yadm/bootstrap). The script should be idempotent, and so can be used via `yadm bootstrap` to (re-)apply the configuration from this repository.

### New machine setup

To bootstrap a machine from first login, we need to manually install `yadm`, then use the `--bootstrap` option when cloning the dotfiles repository. Alternatively, if the repository has already been cloned, running `yadm bootstrap` will suffice, as well.

1. Install the [Homebrew package manager](https://brew.sh/)
1. Install `yadm` manually, according to the [upstream documentation](https://yadm.io/docs/install#osx): `brew install yadm`
1. Clone this project and run the bootstrap script: `yadm clone --bootstrap https://gitlab.com/craig/dotfiles.git`

